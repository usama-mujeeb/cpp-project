#include "database.h"
#include "account_data.h"
#include "personal_data.h"
#include "nlohmann/json.hpp"
#include <fstream>
using json = nlohmann::json;

// read a JSON file
json emumba::training::database::read_json(const std::string file_path = "../data/personal_data.json")
{
    std::ifstream infile(file_path);
    return json::parse(infile);
}

bool emumba::training::database::initialize_database()
{
    personal_data personal_info;
    account_data account_info;
    std::string account_id;
    try
    {
        json json_data = read_json();
        for (uint8_t account_holder = 0; json_data["Account Holders Info"].size() != account_holder; ++account_holder)
        {
            personal_info.set_age(json_data["Account Holders Info"][account_holder]["age"]);
            personal_info.set_city(json_data["Account Holders Info"][account_holder]["city"]);
            personal_info.set_coordinates(json_data["Account Holders Info"][account_holder]["coordinates"]["lat"],
                                          json_data["Account Holders Info"][account_holder]["coordinates"]["long"]);
            _personal_data_list.push_back(personal_info);
            account_info.set_name(json_data["Account Holders Info"][account_holder]["name"]);
            _personal_info_map[account_info.get_name()] = &_personal_data_list.back();
            for (uint8_t account = 0; json_data["Account Holders Info"][account_holder]["accounts"].size() != account; ++account)
            {
                account_id = json_data["Account Holders Info"][account_holder]["accounts"][account]["account id"];
                account_info.set_bank(json_data["Account Holders Info"][account_holder]["accounts"][account]["bank"]);
                account_info.set_account_balance(json_data["Account Holders Info"][account_holder]["accounts"][account]["balance"]);
                account_info.set_currency(json_data["Account Holders Info"][account_holder]["accounts"][account]["currency"]);
                _account_data_list.push_back(account_info);
                _accounts_info_map[account_id] = &_account_data_list.back();
            }
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        return false;
    }
    return true;
}

bool emumba::training::database::is_person_present(const std::string &name)
{
    return (_personal_info_map.find(name) != _personal_info_map.end());
}

bool emumba::training::database::is_account_present(const std::string &account_id)
{
    return (_accounts_info_map.find(account_id) != _accounts_info_map.end());
}

const emumba::training::query_struct emumba::training::database::read_query(const std::string &file_path = "../data/query.txt")
{
    query_struct query;
    size_t size;
    std::fstream infile(file_path);
    getline(infile, query.key, ':');
    getline(infile, query.value);
    query.key = query.key.substr(query.key.find('"') + 1, query.key.rfind('"') - 1);
    query.value = query.value.substr(query.value.find('"') + 1, query.value.rfind('"') - 1);
    return query;
}

void emumba::training::database::pool_queries()
{
    query_struct query = read_query();
    if (query.key == "name" && is_person_present(query.value))
    {
        json json_data = {{"name", query.value},
                          {"age", _personal_info_map[query.value]->get_age()},
                          {"city", _personal_info_map[query.value]->get_city()},
                          {"coordinates", {{"lat", _personal_info_map[query.value]->get_latitude()}, {"long", _personal_info_map[query.value]->get_longitude()}

                                          }}};
        std::ofstream outfile("../query_result.json");
        outfile << json_data.dump(4); //indent = 4
    }
}
