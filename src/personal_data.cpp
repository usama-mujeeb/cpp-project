#include "personal_data.h"
using namespace emumba::training;

// Constructor
personal_data::personal_data()
{
    set_age(0);
    set_city("NULL");
    set_coordinates(0.0, 0.0);
}

personal_data::personal_data(const uint8_t &age, const std::string &city, const float &latitude, const float &longitude)
{
    set_age(age);
    set_city(city);
    set_coordinates(latitude, longitude);
}

// Setter Functions
bool personal_data::set_age(const uint8_t &age)
{
    if (age >= 18 && age <= 100)
    {
        _person_data.age = age;
        return true;
    }
    return false;
}

void personal_data::set_city(const std::string &city)
{
    _person_data.city = city;
}

bool personal_data::set_coordinates(const std::vector<float> &coordinates)
{
    if (coordinates.size() == 2)
        return set_coordinates(coordinates[0], coordinates[1]);
    return false;
}

bool personal_data::set_coordinates(const float &latitude, const float &longitude)
{
    if (latitude >= -90 && latitude <= 90 && longitude >= -180 && longitude <= 180)
    {
        _person_data.coordinates[0] = latitude;
        _person_data.coordinates[1] = longitude;
        return true;
    }
    return false;
}

// Getter Functions
const std::uint8_t &personal_data::get_age() const
{
    return _person_data.age;
}

const std::string &personal_data::get_city() const
{
    return _person_data.city;
}

const std::vector<float> &personal_data::get_coordinates() const
{
    return _person_data.coordinates;
}

const float &personal_data::get_latitude() const
{
    return _person_data.coordinates[0];
}

const float &personal_data::get_longitude() const
{
    return _person_data.coordinates[1];
}

//Display Function
void personal_data::print_personal_data()
{
    std::vector<float> coordinates = get_coordinates();
    std::cout << "\n--------------------------------------------\n";
    std::cout << "Age: " << (uint16_t)get_age() << std::endl;
    std::cout << "City: " << get_city() << std::endl;
    std::cout << "Latitude: " << coordinates[0] << std::endl;
    std::cout << "longitude: " << coordinates[1] << std::endl;
    std::cout << "\n--------------------------------------------\n";
}