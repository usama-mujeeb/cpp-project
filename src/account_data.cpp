#include "account_data.h"
#include <iostream>
using namespace emumba::training;

// Constructor
account_data::account_data()
{
    set_name("NULL");
    set_bank("NULL");
    set_account_balance(0.0);
    set_currency("NIL");
}

account_data::account_data(const std::string &name, const std::string &bank, const float &account_balance, const std::string &currency)
{
    set_name(name);
    set_bank(bank);
    set_account_balance(account_balance);
    set_currency(currency);
}

// Setter Functions
void account_data::set_name(const std::string &name)
{
    _account_data.name = name;
}

void account_data::set_bank(const std::string &bank)
{
    _account_data.bank = bank;
}

bool account_data::set_account_balance(const float &account_balance)
{
    if (account_balance >= 0)
    {
        _account_data.account_balance = account_balance;
        return true;
    }
    return false;
}

bool account_data::set_currency(const std::string &currency)
{
    if (currency.length() == 3)
    {
        _account_data.currency = currency;
        return true;
    }
    return false;
}

// Getter Functions
const std::string &account_data::get_name() const
{
    return _account_data.name;
}

const std::string &account_data::get_bank() const
{
    return _account_data.bank;
}

const float &account_data::get_account_balance() const
{
    return _account_data.account_balance;
}

const std::string &account_data::get_currency() const
{
    return _account_data.currency;
}

// Display Function
void account_data::print_account_data()
{
    std::cout << "\n--------------------------------------------\n";
    std::cout << "Name: " << get_name() << std::endl;
    std::cout << "Bank: " << get_bank() << std::endl;
    std::cout << "Balance: " << get_account_balance() << std::endl;
    std::cout << "Currency: " << get_currency() << std::endl;
    std::cout << "\n--------------------------------------------\n";
}