#ifndef _ACCOUNT_DATA_H_
#define _ACCOUNT_DATA_H_
#include <string>

namespace emumba::training
{
    struct account_data_struct
    {
        std::string name;
        std::string bank;
        float account_balance;
        std::string currency;
    };

    class account_data
    {
    private:
        account_data_struct _account_data;

    public:
        // Constructor
        account_data();
        account_data(const std::string &name, const std::string &bank, const float &account_balance, const std::string &currency);
        // Setter Functions
        void set_name(const std::string &name);
        void set_bank(const std::string &bank);
        bool set_account_balance(const float &account_balance);
        bool set_currency(const std::string &currency);
        // Getter Functions
        const std::string &get_name() const;
        const std::string &get_bank() const;
        const float &get_account_balance() const;
        const std::string &get_currency() const;
        // Display Function
        void print_account_data();
    };
};
#endif