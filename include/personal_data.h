#ifndef _PERSONAL_DATA_H_
#define _PERSONAL_DATA_H_
#include <iostream>
#include <vector>

namespace emumba::training
{
    struct personal_data_struct
    {
        uint8_t age;
        std::string city;
        std::vector<float> coordinates = std::vector<float>(2);
        ;
    };
    class personal_data
    {
    private:
        personal_data_struct _person_data;

    public:
        // Constructor
        personal_data();
        personal_data(const uint8_t &age, const std::string &city, const float &latitude, const float &longitude);
        // Setter Functions
        bool set_age(const uint8_t &age);
        void set_city(const std::string &city);
        bool set_coordinates(const std::vector<float> &coordinates);
        bool set_coordinates(const float &latitude, const float &longitude);
        // Getter Functions
        const std::uint8_t &get_age() const;
        const std::string &get_city() const;
        const std::vector<float> &get_coordinates() const;
        const float &get_latitude() const;
        const float &get_longitude() const;
        //Display Function
        void print_personal_data();
    };
};
#endif