#ifndef DATABASE_H
#define DATABASE_H
#include <map>
#include <list>
#include "personal_data.h"
#include "account_data.h"
#include "nlohmann/json.hpp"

namespace emumba::training
{
    struct query_struct
    {
        std::string key;
        std::string value;
    };
    class database
    {
    private:
        std::list<personal_data> _personal_data_list;
        std::list<account_data> _account_data_list;
        std::map<std::string /*person's name*/, personal_data *> _personal_info_map;
        std::map<std::string /*account id*/, account_data *> _accounts_info_map;

    public:
        nlohmann::json read_json(const std::string file_path);
        bool initialize_database();
        void pool_queries();
        bool is_person_present(const std::string &name);
        bool is_account_present(const std::string &account_id);
        const query_struct read_query(const std::string &file_path);
    };
}
#endif