#include "database.h"
#include "personal_data.h"
#include "account_data.h"

using namespace emumba::training;

int main()
{
    database bank_database;
    bank_database.initialize_database();
    bank_database.pool_queries();
}